module.exports = {
  runtimeCompiler: true,
  pluginOptions: {
    i18n: {
      locale: "de",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: false
    }
  }
};
