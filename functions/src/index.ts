import { database, pubsub, https } from "firebase-functions";
import {
  initializeApp,
  database as adminDatabase,
  auth as adminAuth,
  storage as adminStorage,
} from "firebase-admin";
import { Player, Game } from "./types";
import { UserRecord } from "firebase-functions/lib/providers/auth";
import asyncPool from "tiny-async-pool";

initializeApp();
const db = adminDatabase();
const auth = adminAuth();
const storage = adminStorage();

/**
 * Creates a new message object in the messages list of a specified game.
 * This method is not exported and can only be invoked by other server functions.
 * @param gameId - The UID of the game to which this message belongs
 * @param authorId - The UID of the author of the game OR a code denoting server messages (-1, -2, -3, -4)
 * @param message - The text of the message
 */
const sendMessage = function(
  gameId: string,
  authorId: number,
  message: string
) {
  db.ref("games/" + gameId + "/messages")
    .push()
    .set({
      authorId: authorId,
      message,
      timestamp: Date.now(),
      isPlayer: false,
    })
    .then(() => {
      console.log(`Pushed message with content ${message}`);
    })
    .catch((error) => {
      console.error(`Error pushing message with content ${message}: `, error);
    });
};

/**
 * Creates a new question object in the question list of a specified game.
 * This method is not exported and can only be invoked by other server functions.
 * @param gameId - The UID of the game to which this question belongs
 * @param authorId - The UID of the player who posed the question
 * @param question - The text of the question
 * @param answer - The result of the voting on the question
 */
const sendQuestion = function(
  gameId: string,
  authorId: string,
  question: string,
  answer: string
) {
  db.ref("games/" + gameId + "/questions")
    .push()
    .set({
      question,
      answer,
      authorId: authorId,
    })
    .then(() => {
      console.log(`Pushed question with content ${question}`);
    })
    .catch((error) => {
      console.error(`Error pushing question with content ${question}: `, error);
    });
};

/**
 * When a player guesses their character, this method checks if all the player have solved their character and
 * if so, marks the game as finished and notifies the players.
 * This method is not exported and can only be invoked by other server functions.
 * @param gameId - The UID of the game to check
 */
const onCharacterSolved = function(gameId: string) {
  db.ref("games/" + gameId + "/players")
    .once("value")
    .then((data) => {
      // Check if all the players have solved their character
      const players = Object.entries(data.val()).map((x) => x[1] as Player);
      const gameFinished = players.every((player: Player) => player.hasSolved);

      if (gameFinished) {
        // Send a message that the game is finished (authorID = -3)
        sendMessage(gameId, -3, "");
        // Mark the game as finished
        db.ref("games/" + gameId)
          .update({
            isFinished: true,
          })
          .then(() => {
            console.log(`Finished game ${gameId}`);
          })
          .catch((error) => {
            console.error(`Error finishing game ${gameId}`, error);
          });
        // Unbind all disconnect events for the players
        unbindOnDisconnect(
          gameId,
          players.map((player: Player) => player.userId)
        );
      }
    })
    .catch((error) => {
      console.error(
        `Error reading field players of game ${gameId} in onCharacterSolved:`,
        error
      );
    });
};

/**
 * Unbinds the disconnect event from one or more specified users and a game. This means that the user will no longer be
 * marked as offline if they disconnect from the game.
 * This method is not exported and can only be invoked by other server functions.
 * @param gameId - The UID of the game to unbind the event from
 * @param userIds - An array of UIDs of users to unbind the event from
 */
const unbindOnDisconnect = function(gameId: string, userIds: Array<string>) {
  for (const userId in userIds) {
    db.ref("games/" + gameId + "/players/" + userId + "/online")
      .onDisconnect()
      .remove()
      .catch((error) =>
        console.error(
          `Error unbinding onDisconnect from user ${userId} in game ${gameId}: `,
          error
        )
      );
  }
};

/**
 * Stops an ongoing voting for a specified game.
 * This method is not exported and can only be invoked by other server functions.
 * @param gameId - The UID of the game for which to stop the voting
 */
const stopVoting = function(gameId: string) {
  db.ref("games/" + gameId + "/voting")
    .set({
      votes: {
        "-1": 0,
        "0": 0,
        "1": 0,
        "2": 0,
        "3": 0,
      },
      ongoing: false,
      question: "",
      authorId: "",
      timestamp: null,
    })
    .then(() => {
      console.log(`Reset voting for game ${gameId}`);
    })
    .catch((error) => {
      console.error(`Error resetting voting for game ${gameId}: `, error);
    });
};

/**
 * Counts the votes in a finished voting, sends the result as a message, and puts the question in the question list.
 * If no votes have been cast, this method sends a message indicating this.
 * This method is not exported and can only be invoked by other server functions.
 * @param gameId
 */
const sendVotingResult = function(gameId: string) {
  let maxVote = "-1"; // If no votes have been cast
  let maxValue = 0;
  db.ref("games/" + gameId + "/voting")
    .once("value")
    .then((data) => {
      stopVoting(gameId);
      // Count the votes
      const voting = data.val();
      for (const vote in voting.votes) {
        const value = voting.votes[vote];
        if (value > maxValue && vote !== "-1") {
          maxVote = vote;
          maxValue = value;
        }
      }
      sendQuestion(gameId, voting.authorId, voting.question, maxVote);
      sendMessage(gameId, -1, maxVote);
      // If the answer is "no" or "solved", it's the next player's turn
      if (maxVote === "1" || maxVote === "2") {
        cyclePlayer(gameId, maxVote);
      }
    })
    .catch((error) => {
      console.error(`Error reading field voting of game ${gameId}: `, error);
    });
};

/**
 * Determines the next player whose turn it is. Updates the list of possible players if the last player has
 * correctly guessed their character.
 * @param gameId - The UID of the game for which to determine the next player
 * @param maxVote - The answer the last player has received to their question
 */
const cyclePlayer = function(gameId: string, maxVote: string) {
  const promisePlayerId = db.ref("games/" + gameId + "/playerId").once("value");
  const promisePlayers = db.ref("games/" + gameId + "/players").once("value");
  Promise.all([promisePlayerId, promisePlayers])
    .then(async (data) => {
      const players = Object.entries(data[1].val()).map((x) => x[1] as Player);
      const playerId = data[0].val();
      console.log("MaxVote:", maxVote);

      // Update player list if player has solved
      if (maxVote === "2") {
        db.ref("games/" + gameId + "/players/" + playerId)
          .update({
            hasSolved: true,
          })
          .then(() => {
            console.log(
              `Updated field hasSolved of player ${playerId} of game ${gameId}`
            );
          })
          .catch((error) => {
            console.error(
              `Error updating field hasSolved of player ${playerId} of game ${gameId}:`,
              error
            );
          });
        // Wait for 1,5 seconds so the player can take a look at their character
        await sleep(1500);
        onCharacterSolved(gameId);
      }

      // Create a list of the UIDs of all the players who have not yet guessed their character
      const playerIds = players
        .filter((player: Player) => !player.hasSolved && player.online)
        .map((player: Player) => player.userId);
      // Determine the next player if there's more than one player left
      if (playerIds.length > 1) {
        const index = playerIds.indexOf(playerId);
        const newPlayerId = playerIds[(index + 1) % playerIds.length];
        const newPlayer = players.filter(
          (player: Player) => player.userId === newPlayerId
        )[0];
        const newPlayerName = newPlayer.userName;
        db.ref("games/" + gameId)
          .update({
            playerId: newPlayerId,
          })
          .then(() => {
            console.log(`Updated field playerId of game ${gameId}`);
            sendMessage(gameId, -4, newPlayerName);
          })
          .catch((error) => {
            console.error(
              `Error updating field playerId of game ${gameId}:`,
              error
            );
          });
      }
    })
    .catch((error) => {
      console.error(
        `Error reading fields playerId and players of game ${gameId}:`,
        error
      );
    });
};

function sleep(milliseconds: number) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

/**
 * Callable function to cycle the player in party mode
 */
export const nextPlayer = https.onCall((data, context) => {
  cyclePlayer(data.gameId, data.vote);
});

/**
 * Restarts a finished game with the same players
 */
export const restartGame = https.onCall((data, context) => {
  const gameId = data.gameId;
  // Set state of game to lobby
  const p1 = db.ref(`games/${gameId}/isFinished`).set(false);
  const p2 = db.ref(`games/${gameId}/isSelecting`).set(false);
  const p3 = db.ref(`games/${gameId}/isStarted`).set(false);
  const p4 = db.ref(`games/${gameId}/playerId`).remove();
  Promise.all([p1, p2, p3, p4]).catch(error => {
    console.error(`Error resetting game state of game ${gameId}:`, error);
  })

  // Remove all player characters
  db.ref(`games/${gameId}/players`)
    .once("value")
    .then((result) => {
      const playerIds = Object.entries(result.val()).map(x => (x[1] as Player).userId);
      playerIds.forEach(playerId => {
        const p5 = db.ref(`games/${gameId}/players/${playerId}/character`).remove();
        const p6 = db.ref(`games/${gameId}/players/${playerId}/characterGiver`).remove();
        const p7 = db.ref(`games/${gameId}/players/${playerId}/hasSolved`).set(false);
        Promise.all([p5, p6, p7]).catch(error => {
          console.error(`Error resetting character of player ${playerId} in game ${gameId}:`, error);
        })
      })
    })
    .catch((error) => {
      console.error(
        `Error reading field players of game ${gameId}:`,
        error
      );
    });
})

/**
 * Increments or decrements the number of current players in a game whenever a player disconnects or a new player
 * joins.
 */
export const onUserOnlineStatus = database
  .ref("games/{gameId}/players/{playerId}/online")
  .onWrite((change, context) => {
    const gameId = context.params.gameId;
    const playerId = context.params.playerId;

    // User has (re-)joined the game
    if (change.after.val()) {
      // Increase currentPlayers by 1
      db.ref("games/" + gameId + "/currentPlayers")
        .transaction((value) => {
          return value + 1;
        })
        .catch((error) =>
          console.error(
            `Error incrementing field currentPlayers of game ${gameId}: `,
            error
          )
        );
      // Send message notifying the other players
      db.ref("games/" + gameId + "/players/" + playerId + "/userName")
        .once("value")
        .then((data) => {
          sendMessage(gameId, -2, data.val());
        })
        .catch((error) =>
          console.error(
            `Error reading field userName of game ${gameId}: `,
            error
          )
        );;

    // User has left the game
    } else if (change.after.val() === false) {
      // Decrease currentPlayers by 1
      db.ref("games/" + gameId + "/currentPlayers")
        .transaction((value) => {
          return value - 1;
        })
        .catch((error) =>
          console.error(
            `Error decrementing field currentPlayers of game ${gameId}: `,
            error
          )
        );
      // Send message notifying the other players
      db.ref("games/" + gameId + "/players/" + playerId + "/userName")
        .once("value")
        .then((data) => {
          sendMessage(gameId, -5, data.val());
        })
        .catch((error) =>
          console.error(
            `Error reading field userName of game ${gameId}: `,
            error
          )
        );
      // Cycle host, if necessary
      db.ref("games/" + gameId + "/creatorId")
        .once("value")
        .then((data) => {
          console.log("Host ID: ", data.val());
          console.log("Player ID:", playerId);
          if (data.val() === playerId) {
            cycleHost(gameId);
          }
        })
        .catch((error) => {
          console.error(
            `Error reading field creatorId of game ${gameId}: `,
            error
          );
        });
    }
  });

/**
 * Determines the new host of a game after the old one has left.
 * @param gameId
 */
function cycleHost(gameId: string) {
  console.log("Cycling host of game ", gameId);
  db.ref("games/" + gameId + "/players")
    .once("value")
    .then((data) => {
      const newHostId = (Object.entries(data.val()).map(x => x[1] as Player).filter(x => x.online))[0].userId;
      db.ref("games/" + gameId + "/creatorId")
        .set(newHostId)
        .catch((error) => {
          console.error(
            `Error updating field creatorId of game ${gameId}: `,
            error
          );
        });
    })
    .catch((error) => {
      console.error(
        `Error reading fields playerId and players of game ${gameId}:`,
        error
      );
    });
}

/**
 * Whenever a player gets a character assigned, this method checks if all players have a character assigned to them and
 * if so, starts the game and randomly selects the first player to begin asking questions.
 */
export const onCharacterAssignment = database
  .ref("games/{gameId}/players/{playerId}/character")
  .onWrite((change, context) => {
    const gameId = context.params.gameId;
    db.ref("games/" + gameId + "/players")
      .once("value")
      .then((data) => {
        // Have all players got a character assigned to them?
        const players = Object.entries(data.val()).map((x) => x[1] as Player);
        const assignmentFinished = players.every(
          (player: Player) => player.character !== undefined
        );
        if (assignmentFinished) {
          // Determine the first player
          const playerIds = players.map((player: Player) => player.userId);
          const playerId =
            playerIds[Math.floor(Math.random() * playerIds.length)];
          const playerName = data.val()[playerId].userName;
          // Start the game
          db.ref("games/" + gameId)
            .update({
              isSelecting: false,
              isStarted: true,
              playerId: playerId,
            })
            .then(() => {
              console.log(`Started game ${gameId}`);
            })
            .catch((error) => {
              console.log(`Error starting game ${gameId}: `, error);
            });
          // Notify the players that the game has started
          sendMessage(gameId, -4, playerName);
        }
      })
      .catch((error) => {
        console.error(`Error reading field players of game ${gameId}: `, error);
      });
  });

/**
 * Whenever a vote is cast, this method checks if all current players have cast their vote and if so, stops the voting
 * and begins counting the result.
 */
export const onVote = database
  .ref("games/{gameId}/voting/total")
  .onWrite((change, context) => {
    const gameId = context.params.gameId;
    db.ref("games/" + gameId + "/currentPlayers")
      .once("value")
      .then(async (data) => {
        if (data.val() - 1 === change.after.val() && change.after.val() !== 0) {
          sendVotingResult(gameId);
        }
      })
      .catch((error) => {
        console.error(
          `Error reading field currentPlayers of game ${gameId}: `,
          error
        );
      });
  });

/**
 * Scheduled function to delete all unused accounts every day at 00:00
 */
export const accountCleanup = pubsub
  .schedule("every day 00:00")
  .onRun(async () => {
    const inactiveUsers = await getInactiveUsers();
    await asyncPool(3, inactiveUsers, deleteInactiveUser);
    console.log("User cleanup finished");
  });

/**
 * Deletes one inactive user from the list.
 * @param userToDelete - The Firebase user to delete
 */
function deleteInactiveUser(userToDelete: UserRecord) {
  return auth
    .deleteUser(userToDelete.uid)
    .then(() => {
      console.log(
        "Deleted user account",
        userToDelete.uid,
        "because of inactivity"
      );
    })
    .catch((error) => {
      console.error(
        "Deletion of inactive user account",
        userToDelete.uid,
        "failed:",
        error
      );
    });
}

/**
 * Returns the list of all inactive users.
 * @param users - The (initially empty) list of inactive users, gets expanded by iterating over pages
 * @param nextPageToken - The token to retrieve the next page of users
 */
async function getInactiveUsers(
  users: Array<UserRecord> = [],
  nextPageToken?: string
): Promise<Array<UserRecord>> {
  const result = await auth.listUsers(1000, nextPageToken);

  // Find users that have not signed in in the last 24 hours.
  const inactiveUsers = result.users.filter(
    (user) =>
      Date.parse(user.metadata.lastSignInTime) <
      Date.now() - 24 * 60 * 60 * 1000
  );

  const newUsers = users.concat(inactiveUsers);
  if (result.pageToken) {
    return getInactiveUsers(newUsers, result.pageToken);
  }

  return newUsers;
}

/**
 * Scheduled function to delete all empty games every day at 00:00
 */
export const gameCleanup = pubsub
  .schedule("every day 00:00")
  .onRun(async () => {
    const emptyGames = await getEmptyGames();
    await asyncPool(3, emptyGames, deleteEmptyGame);
    console.log("Game cleanup finished");
  });

function deleteEmptyGame(gameToDelete: Game) {
  if (gameToDelete.players) {
    const imagesToDelete = flatMap(
      (x: [string, Player]) => (x[1].image ? x[1].image : []),
      Object.entries(gameToDelete.players)
    );
    console.log("imagesToDelete:", imagesToDelete);
    for (const image of imagesToDelete) {
      storage
        .bucket()
        .file(`images/${gameToDelete.uid}/${image}`)
        .delete()
        .catch((e) => {
          console.error(`Deletion of image ${image} failed: ${e}`);
        });
    }
  }
  return db.ref(`games/${gameToDelete.uid}`).remove();
}

async function getEmptyGames(): Promise<Array<Game>> {
  const result = await db.ref("games").once("value");
  return flatMap((x: [string, Game]) => {
    if (x[1].currentPlayers <= 0) {
      x[1].uid = x[0];
      return x[1];
    } else {
      return [];
    }
  }, Object.entries(result.val()));
}

const flatMap = (f: Function, xs: Array<any>) =>
  xs.reduce((acc, x) => acc.concat(f(x)), []);
