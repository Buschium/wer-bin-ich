export interface Player {
    character: string | undefined;
    characterGiver: string | undefined;
    hasSolved: boolean;
    image: string | undefined;
    online: boolean;
    userId: string;
    userName: string;
}

export interface Question {
    answer: string;
    authorId: string;
    question: string;
}

export interface Message {
    authorId: string | number;
    isPlayer: boolean;
    message: string;
    timestamp: number;
}

export interface Voting {
    authorId: string;
    ongoing: boolean;
    question: string;
    total: number;
    votes: {
        [index: string]: number;
    };
}

export interface Game {
    uid: string;
    creatorId: string;
    currentPlayers: number;
    isFinished: boolean;
    isSelecting: boolean;
    isStarted: boolean;
    maxPlayers: number;
    messages: {
        [index: string]: Message;
    };
    playerId: string;
    players: {
        [index: string]: Player;
    };
    questions: {
        [index: string]: Question;
    };
    voting: Voting;
}
