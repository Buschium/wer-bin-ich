import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";
import "firebase/storage";
import "firebase/analytics";
import "firebase/functions";

// Firebase configuration object retrieved from the Firebase console
const firebaseConfig = {
  apiKey: "AIzaSyCuhooVyPrsSZyo8X_ohh6yVYkLyIVR45I",
  authDomain: "wer-bin-ich-277818.firebaseapp.com",
  databaseURL: "https://wer-bin-ich-277818.firebaseio.com",
  projectId: "wer-bin-ich-277818",
  storageBucket: "wer-bin-ich-277818.appspot.com",
  messagingSenderId: "797518384585",
  appId: "1:797518384585:web:256cc32770d33158372c18",
  measurementId: "G-1P63C1E3NW"
};

// Export realtime database, authentication, storage, and functions objects
const instance = firebase.initializeApp(firebaseConfig);
export const db = instance.database();
export const auth = instance.auth();
export const storage = instance.storage();
export const functions = instance.functions();

/**
 * Export function to create analytics object
 */
export function getAnalytics() {
  return instance.analytics();
}

/**
 * Deletes all cookies by setting their expiration date to the current date and time
 */
export function deleteAllCookies() {
  document.cookie.split(";").forEach(c => {
    document.cookie = c
      .replace(/^ +/, "")
      .replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
  });
}
