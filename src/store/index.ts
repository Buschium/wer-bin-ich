import Vue from "vue";
import Vuex from "vuex";
import { vuexfireMutations, firebaseAction } from "vuexfire";
import { db, auth, storage, functions } from "../firebase";
import { User } from "firebase";
import { Player } from "@/types";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null, // Firebase user object for the current user
    showGameModal: false, // Whether the modal for creating a new game is currently shown
    acceptCookies: false, // Cookie consent
    isMobile: false, // Whether the page is accessed from a mobile device
    isTouch: false, // Whether the page is accessed from a touchscreen (mobile or tablet) device
    online: true, // Whether there is a connection to Firebase
    game: null, // The currently loaded game object
    allGames: null // The list of all games, only bound in the games list view
  },

  mutations: {
    /**
     * Stores a user object in the state
     * @param state
     * @param user - A {@link https://firebase.google.com/docs/auth/users|Firebase User Object} to store
     */
    setUser(state: any, user: User) {
      state.user = user;
    },

    /**
     * Stores the visibility status of the modal to create a new game in the state
     * @param state
     * @param showGameModal - Whether the modal should be shown
     */
    setShowGameModal(state: any, showGameModal: boolean) {
      state.showGameModal = showGameModal;
    },

    /**
     * Stores the current connection status in the state
     * @param state
     * @param online - Whether the client has an active connection to Firebase
     */
    setOnline(state: any, online: boolean) {
      state.online = online;
    },

    /**
     * Stores the cookie consent status in the state and in Local Storage
     * @param state
     * @param acceptCookies - Whether cookies are accepted
     */
    setAcceptCookies(state: any, acceptCookies: boolean) {
      if (acceptCookies) {
        console.log("Accepted cookies");
      } else {
        console.log("Declined cookies");
      }
      state.acceptCookies = acceptCookies;
      localStorage.setItem("cookieStatus", String(acceptCookies));
    },

    /**
     * Stores the device type in the state
     * @param state
     * @param isMobile - Whether the website is accessed from a mobile device
     */
    setIsMobile(state: any, isMobile: boolean) {
      state.isMobile = isMobile;
    },

    /**
     * Stores the device type in the state
     * @param state
     * @param isTouch - Whether the website is accessed from a touch device
     */
    setIsTouch(state: any, isTouch: boolean) {
      state.isTouch = isTouch;
    },

    /**
     * Restores the cookie consent status from Local Storage
     * @param state
     */
    restoreState(state: any) {
      if (localStorage.getItem("cookieStatus")) {
        state.acceptCookies = localStorage.getItem("cookieStatus") === "true";
      }
    },
    ...vuexfireMutations
  },

  actions: {
    /**
     * Bind the store to the Firebase Realtime Database object for the current game so changes are synchronized automatically
     * @param gameId - The UID of the game to connect to
     * @public
     */
    bindGame: firebaseAction(({ bindFirebaseRef }, gameId: string) => {
      return bindFirebaseRef("game", db.ref("games/" + gameId));
    }),

    /**
     * Bind the store to the games in the Firebase Realtime Database
     * @public
     */
    bindAllGames: firebaseAction(({ bindFirebaseRef }) => {
      return bindFirebaseRef("allGames", db.ref("games"));
    }),

    /**
     * Unbind the store from the games in the Firebase Realtime Database to lighten computational load
     * @public
     */
    unbindAllGames: firebaseAction(({ unbindFirebaseRef }) => {
      return unbindFirebaseRef("allGames");
    }),

    /**
     * Determines the behaviour of the application on connect and disconnect.
     * On connect: Mark the user as online and set up an event that marks him as offline on disconnect.
     * On disconnect: The previously set up event gets triggered on the server.
     * @param data - An object containing the UIDs of the game and the user
     * @public
     */
    bindOnDisconnect: firebaseAction((context: any, data: any) => {
      // Bind client callback
      db.ref(".info/connected").on("value", snap => {
        if (snap.val()) {
          // Set online = true
          const onlineRef = db.ref(
            "games/" + data.gameId + "/players/" + data.uid + "/online"
          );
          onlineRef.set(true);

          // Set online = false on disconnect
          onlineRef
            .onDisconnect()
            .set(false)
            .catch(error => {
              console.error("Could not establish onDisconnect event: ", error);
            });
        }
        context.commit("setOnline", snap.val());
      });
    }),

    unbindOnDisconnect: firebaseAction((context: any, data: any) => {
      db.ref("games/" + data.gameId + "/players/" + data.userId + "/online")
        .onDisconnect()
        .remove()
        .catch(error =>
          console.error(
            `Error unbinding onDisconnect from user ${data.userId} in game ${data.gameId}: `,
            error
          )
        );
    }),

    /**
     * Binds a listener to the authorization state of the current user and updates the user variable accordingly
     * @public
     */
    bindAuthStateListener: (context: any) => {
      auth.onAuthStateChanged(user => {
        if (user) {
          context.commit("setUser", user);
        } else {
          context.commit("setUser", null);
        }
      });
    },

    /**
     * Pushes a new message to Firebase Realtime Database and starts the voting process if the message comes
     * from the current player and contains a question mark ("?")
     * @param data - An object containing a message object (the actual message and the author's UID)
     * @public
     */
    pushMessage: firebaseAction((context: any, data: any) => {
      db.ref("games/" + context.state.game[".key"] + "/messages")
        .push()
        .set(data.message);
      const isPlayer = context.state.user.uid === context.state.game.playerId;
      if (isPlayer && data.message.message.indexOf("?") !== -1) {
        // Start voting if the message comes from the current player and contains a question mark ("?")
        db.ref("games/" + context.state.game[".key"] + "/voting").update({
          question: data.message.message,
          authorId: data.message.authorId,
          ongoing: true,
          timestamp: Date.now()
        });
      }
    }),

    /**
     * Starts the game the state is currently bound to
     * @public
     */
    startGame: firebaseAction((context: any) => {
      db.ref("games/" + context.state.game[".key"] + "/isStarted").set(true);
    }),

    /**
     * Starts the character assignment process for the game the state is currently bound to
     * @public
     */
    startSelecting: firebaseAction((context: any) => {
      db.ref("games/" + context.state.game[".key"] + "/isSelecting").set(true);
    }),

    /**
     * Starts the character assignment process for a specific user and "reserves" them so no other user can assign
     * a character to them
     * @param data - An object containing the UID of the user a character will be assigned to
     * @public
     */
    startCharacterAssignment: firebaseAction((context: any, data: any) => {
      db.ref(
        "games/" +
          context.state.game[".key"] +
          "/players/" +
          data.uid +
          "/characterGiver"
      ).set(context.state.user.uid);
    }),

    /**
     * Cancels the character assignment process for a specific user and lifts the reservation on this user
     * @param data - An object containing the UID of the user for whom the character assignment is cancelled
     * @public
     */
    stopCharacterAssignment: firebaseAction((context: any, data: any) => {
      db.ref(
        "games/" +
          context.state.game[".key"] +
          "/players/" +
          data.uid +
          "/characterGiver"
      ).remove();
    }),

    /**
     * Assigns a character to a user
     * @param data - An object containing the character and the UID of the user this character gets assigned to
     * @public
     */
    assignCharacter: firebaseAction((context: any, data: any) => {
      db.ref(
        "games/" +
          context.state.game[".key"] +
          "/players/" +
          data.uid +
          "/character"
      ).set(data.character);
    }),

    /**
     * Adds a new user to a game and fires the bindOnDisconnect event
     * @see {@link bindOnDisconnect}
     * @param data - An object containing the UID and user name of the user as well as the UID of the game this user has joined
     * @public
     */
    addUser: firebaseAction((context: any, data: any) => {
      if (!data.gameId) {
        data.gameId = context.state.game[".key"];
      }
      if (context.state.game) {
        const players = Object.entries(context.state.game.players);
        if (players.length === context.state.game.maxPlayers) {
          const offlineId = players
            .map(x => x[1] as Player)
            .filter(x => !x.online)[0].userId;
          db.ref("games/" + data.gameId + "/players/" + offlineId)
            .remove()
            .catch(error =>
              console.error("Error removing offline player: ", error)
            );
        }
      }
      db.ref("games/" + data.gameId + "/players/" + data.uid)
        .set({
          userId: data.uid,
          userName: data.userName,
          hasSolved: false,
          online: true
        })
        .then(() => {
          context.dispatch("bindOnDisconnect", {
            gameId: data.gameId,
            uid: data.uid
          });
        })
        .catch(error => {
          console.error("Error adding player: ", error);
        });
    }),

    /**
     * Removes a user from a game and fires the unbindOnDisconnect event
     * @see {@link bindOnDisconnect}
     * @param data - An object containing the UID and user name of the user as well as the UID of the game this user has left
     * @public
     */
    removeUser: firebaseAction((context: any, data: any) => {
      if (!data.gameId) {
        data.gameId = context.state.game[".key"];
      }

      db.ref("games/" + data.gameId + "/players/" + data.uid + "/online")
        .set(false)
        .then(() => {
          context.dispatch("unbindOnDisconnect", {
            gameId: data.gameId,
            userId: data.uid
          });
        })
        .catch(error => {
          console.error("Error removing player: ", error);
        });
    }),

    /**
     * Creates a new game
     * @param data - An object containing the UID and user name of the game creator as well as the maximum amount of players
     * @public
     */
    createGame: firebaseAction((context: any, data: any) => {
      const ref = db.ref("games");
      return ref
        .push({
          creatorId: data.uid,
          currentPlayers: 0,
          isFinished: false,
          isSelecting: false,
          isStarted: false,
          maxPlayers: data.maxPlayers,
          partyMode: data.partyMode,
          privateGame: data.privateGame,
          voting: {
            votes: {
              "-1": 0,
              "0": 0,
              "1": 0,
              "2": 0,
              "3": 0
            },
            ongoing: false,
            question: "",
            authorId: "",
            total: 0
          }
        })
        .then(game => {
          context.dispatch("addUser", {
            gameId: game.key,
            uid: data.uid,
            userName: data.userName
          });
          context.commit("setShowGameModal", false);
          return game.key;
        });
    }),

    /**
     * Casts a vote for a question (yes, no, solved, or invalid)
     * @param voting - An integer (0, 1, 2, or 3) indicating the vote cast
     * @public
     */
    pushVoting: firebaseAction((context: any, voting: number) => {
      const ref = db.ref("/games/" + context.state.game[".key"] + "/voting");
      switch (voting) {
        case 0:
          ref.child("votes/0").transaction(yes => {
            return yes + 1;
          });
          break;
        case 1:
          ref.child("votes/1").transaction(no => {
            return no + 1;
          });
          break;
        case 2:
          ref.child("votes/2").transaction(solved => {
            return solved + 1;
          });
          break;
        case 3:
          ref.child("votes/3").transaction(invalid => {
            return invalid + 1;
          });
          break;
        default:
          ref.child("votes/-1").transaction(noVote => {
            return noVote + 1;
          });
          break;
      }
      ref.child("total").transaction(total => {
        return total + 1;
      });
    }),

    /**
     * Uploads an image to Firebase storage and binds a reference to that image to the corresponding user.
     * @param data - An object containing the image and the UID of the user this image gets assigned to
     * @public
     */
    uploadImage: firebaseAction(
      (context: any, data: { file: File; uid: string }) => {
        storage
          .ref("images/" + context.state.game[".key"])
          .child(data.file.name)
          .put(data.file)
          .then(function() {
            db.ref(
              "games/" +
                context.state.game[".key"] +
                "/players/" +
                data.uid +
                "/image"
            ).set(data.file.name);
          });
      }
    ),

    /**
     * Tell the server to find the next player
     * Votes: 1 = player hasn't guessed their character
     * 2 = player has guessed their character
     */
    nextPlayer: firebaseAction((context: any, vote: string) => {
      const func = functions.httpsCallable("nextPlayer");
      func({ gameId: context.state.game[".key"], vote: vote });
    }),

    /**
     * Tell the server to restart a game
     */
    restartGame: firebaseAction((context: any) => {
      const func = functions.httpsCallable("restartGame");
      func({ gameId: context.state.game[".key"] });
    })
  }
});
