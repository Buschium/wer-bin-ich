import Vue from "vue";
import VueRouter, { Route } from "vue-router";
import Game from "../views/Game.vue";
import GamesList from "@/views/GamesList.vue";
import Home from "../views/Home.vue";
import Privacy from "../views/Privacy.vue";
import Imprint from "../views/Imprint.vue";
import Feedback from "@/views/Feedback.vue";
import Roadmap from "@/views/Roadmap.vue";
import Maintenance from "@/views/Maintenance.vue";
import store from "../store";
import i18n from "../i18n";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    beforeEnter: (to: Route, from: Route, next: Function) => {
      // Before entering the page, check the current login status
      store.dispatch("bindAuthStateListener").then(() => {
        next();
      });
    },
    meta: {
      title: i18n.t("ui.homePageTitle")
    }
  },
  {
    path: "/game/:gameId",
    name: "Game",
    component: Game,
    beforeEnter: (to: Route, from: Route, next: Function) => {
      // Before entering the page, retrieve the game data from Firebase and check the current login status
      Promise.all([
        store.dispatch("bindGame", to.params.gameId),
        store.dispatch("bindAuthStateListener")
      ]).then(() => {
        next();
      });
    },
    meta: {
      title: i18n.t("ui.gamePageTitle")
    }
  },
  {
    path: "/games",
    name: "Games",
    component: GamesList,
    beforeEnter: (to: Route, from: Route, next: Function) => {
      Promise.all([store.dispatch("bindAllGames")]).then(() => {
        next();
      });
    },
    meta: {
      title: i18n.t("ui.gamesListTitle")
    }
  },
  {
    path: "/privacy",
    name: "Privacy Policy",
    component: Privacy,
    meta: {
      title: i18n.t("ui.privacyPageTitle")
    }
  },
  {
    path: "/imprint",
    name: "Imprint",
    component: Imprint,
    meta: {
      title: i18n.t("ui.imprintPageTitle")
    }
  },
  {
    path: "/feedback",
    name: "Feedback",
    component: Feedback,
    meta: {
      title: i18n.t("ui.feedbackPageTitle")
    }
  },
  {
    path: "/roadmap",
    name: "Roadmap",
    component: Roadmap,
    meta: {
      title: i18n.t("ui.roadmapPageTitle")
    }
  }
];

/**
 * Create a new router object in history mode (without the "?" in routes)
 */
const router = new VueRouter({
  mode: "history",
  routes
});

/**
 * A route guard for updating the page title in the tab
 */
router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.title);
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title;
  next();
});

export default router;
