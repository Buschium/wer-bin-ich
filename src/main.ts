import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";
import {
  fas,
  faTimes,
  faCheck,
  faUser,
  faUserCheck,
  faBan,
  faQuestionCircle,
  faCheckCircle,
  faTimesCircle,
  faPaperPlane,
  faPlug
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import Buefy from "buefy";
import store from "./store";
import PerfectScrollbar from "vue2-perfect-scrollbar";
import VueCookieAcceptDecline from "vue-cookie-accept-decline";
import Vue2TouchEvents from "vue2-touch-events";

import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";
import "vue-cookie-accept-decline/dist/vue-cookie-accept-decline.css";
import i18n from "./i18n";
import FlagIcon from "vue-flag-icon";

// Add icons to library
library.add(faGitlab);
library.add(
  fas,
  faTimes,
  faCheck,
  faUser,
  faUserCheck,
  faBan,
  faQuestionCircle,
  faCheckCircle,
  faTimesCircle,
  faPaperPlane,
  faPlug
);

// Add custom components
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("vue-cookie-accept-decline", VueCookieAcceptDecline);

// Add Vue extensions
Vue.use(Buefy, {
  defaultIconPack: "fas"
});
Vue.use(PerfectScrollbar, { options: { wheelPropagation: false } });
Vue.use(Vue2TouchEvents, { swipeTolerance: 20 });
Vue.use(FlagIcon);

Vue.config.productionTip = false;

// Create Vue app
new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
