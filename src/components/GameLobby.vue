<template>
  <!-- The swipe gestures are handled by the top container so they work everywhere on the page -->
  <!-- The desktop version isn't affected by the swipe events -->
  <div v-touch:swipe.right="onSwipeRight" v-touch:swipe.left="onSwipeLeft">
    <!-- Popup highlighting the player list -->
    <div v-if="showPlayerList" class="popup" @click="onSwipeLeft"></div>

    <!-- Game lobby for touch devices. Shows the chat over the whole width of the screen with the player list off
    screen to the left. The player list can be dragged into view by swiping to the right -->
    <div v-if="isTouch" class="tile is-ancestor is-mobile game--lobby">
      <!-- Player list / menu -->
      <div
        class="tile is-parent side-left"
        :class="{ expanded: showPlayerList }"
      >
        <div class="tile is-child">
          <player-menu v-if="isSelecting" />
          <player-list v-else />
        </div>
      </div>

      <!-- Center column that is always shown, contains the "start game" button and the chat -->
      <div class="tile is-parent is-vertical game--center">
        <div
          class="tile is-child box start-game"
          :class="{
            // A button for the creator when there are enough players and the character assignment isn't ongoing
            'start-game-button': isCreator && !isEmpty && !hasAssigned,
            // A span otherwise
            'start-game-span': !isCreator || isEmpty || hasAssigned
          }"
        >
          <!-- If the character assignment is ongoing, tell the user to wait for the other players -->
          <span v-if="isSelecting || hasAssigned">{{
            $t("ui.waitForOtherPlayers")
          }}</span>

          <!-- If the character assignment is not ongoing, there are enough players, and the user is the creator,
          show the user the button to start the character assignment -->
          <a v-else-if="isCreator && !isEmpty" @click="startSelecting">
            <span>{{ $t("ui.startGame") }}</span>
          </a>

          <!-- If there are not enough players (i.e. less than two), tell the user to wait for more player -->
          <span v-else-if="isEmpty">{{ $t("ui.waitForMorePlayers") }}</span>

          <!-- Else tell the user to wait for the game creator to start the game -->
          <span v-else>{{ $t("ui.waitForGameStart") }}</span>
        </div>
        <div class="tile is-child">
          <game-chat />
        </div>
      </div>

      <!-- The link to this game -->
      <div class="tile is-parent">
        <div class="tile is-child">
          <b-message v-if="!isStarted" class="link-message" type="is-primary">
            <i18n path="ui.linkToThisGame" tag="p">
              <a>{{ $t("ui.gameLink", [currentRoute]) }}</a>
            </i18n>
          </b-message>
        </div>
      </div>
    </div>

    <!-- The game lobby for desktop devices. Only differs in terms of layout (the player list and center column are
    shown side by side), not functionality -->
    <div v-else class="tile is-ancestor is-vertical game--lobby">
      <div class="tile game--center">
        <div class="tile is-parent is-4">
          <div class="tile is-child">
            <player-menu v-if="isSelecting" />
            <player-list v-else />
          </div>
        </div>
        <div class="tile is-parent is-vertical is-8">
          <div
            class="tile is-child box start-game"
            :class="{
              'start-game-button': isCreator && !isEmpty && !hasAssigned,
              'start-game-span': !isCreator || isEmpty || hasAssigned
            }"
          >
            <span v-if="hasAssigned">{{ $t("ui.waitForOtherPlayers") }} </span>
            <a v-else-if="isCreator && !isEmpty" @click="startSelecting">
              <span>{{ $t("ui.startGame") }}</span>
            </a>
            <span v-else-if="isEmpty">{{ $t("ui.waitForMorePlayers") }}</span>
            <span v-else>{{ $t("ui.waitForGameStart") }}</span>
          </div>
          <div class="tile is-child">
            <game-chat />
          </div>
        </div>
      </div>
      <div class="tile is-parent">
        <div class="tile is-child">
          <b-message v-if="!isStarted" class="link-message" type="is-primary">
            <i18n path="ui.linkToThisGame" tag="p">
              <a>{{ $t("ui.gameLink", [currentRoute]) }}</a>
            </i18n>
          </b-message>
        </div>
      </div>
    </div>
  </div>
</template>

<script lang="ts">
import { Component, Watch, Vue } from "vue-property-decorator";
import { Player } from "@/types";
import GameChat from "@/components/GameChat.vue";
import PlayerList from "@/components/PlayerList.vue";
import PlayerMenu from "@/components/PlayerMenu.vue";
import SelectionModal from "@/components/SelectionModal.vue";
import firebase from "firebase";

@Component({
  components: {
    GameChat,
    PlayerList,
    PlayerMenu
  }
})
export default class GameLobby extends Vue {
  // Data
  /**
   * Whether the player list / menu has been dragged into view, for mobile.
   * @private
   */
  private showPlayerList = false;

  // Computed properties
  /**
   * Whether the page is being viewed on a touchscreen device (mobile or tablet).
   */
  get isTouch(): boolean {
    return this.$store.state.isTouch;
  }

  /**
   * Returns the players in this game as an array.
   */
  get players(): Array<Player> {
    return Object.entries(this.$store.state.game.players).map(
      x => x[1] as Player
    );
  }

  /**
   * Whether the current user has selected another player they want to assign a character to,
   * but not yet made the assignment.
   */
  get hasSelected(): boolean {
    return this.selectedPlayer !== undefined && !this.hasAssigned;
  }

  /**
   * Returns the player which the current user has selected for the character assignment.
   */
  get selectedPlayer(): Player | undefined {
    return this.players.filter(
      player =>
        player.characterGiver !== undefined &&
        player.characterGiver === this.userId
    )[0];
  }

  /**
   * Whether the current user has assigned a character to another player.
   */
  get hasAssigned(): boolean {
    const assignee = this.players.filter(
      (player: Player) => player.characterGiver === this.userId
    )[0];
    return assignee !== undefined && assignee.character !== undefined;
  }

  /**
   * Whether the game has been started.
   */
  get isStarted(): boolean {
    return this.$store.state.game.isStarted;
  }

  /**
   * Whether the game is in the character assignment phase and the current user is searching for
   * another player to assign a character to.
   */
  get isSelecting(): boolean {
    return this.$store.state.game.isSelecting && !this.hasAssigned;
  }

  /**
   * Whether the current user is the creator of the current game.
   */
  get isCreator(): boolean {
    if (this.isLoggedIn) {
      return this.$store.state.game.creatorId === this.$store.state.user.uid;
    } else {
      return false;
    }
  }

  /**
   * Returns the UID of the current user.
   */
  get userId(): string | undefined {
    if (this.isLoggedIn) {
      return this.$store.state.user.uid;
    } else {
      return undefined;
    }
  }

  /**
   * Whether the current user is logged in, i.e. an anonymous Firebase user has been created.
   */
  get isLoggedIn(): boolean {
    return this.$store.state.user !== null;
  }

  /**
   * Whether the current user has already joined the game (important for page reloads etc.).
   */
  get hasJoinedGame(): boolean {
    return (
      this.isLoggedIn &&
      this.players.filter(
        (player: Player) => player.userId === this.$store.state.user.uid
      )[0] !== undefined
    );
  }

  /**
   * Whether the maximum number of players for the current game has been reached.
   */
  get isFull(): boolean {
    return (
      this.$store.state.game.maxPlayers ===
      this.$store.state.game.currentPlayers
    );
  }

  /**
   * Whether there are too few players to start a game, i.e. none or just one.
   */
  get isEmpty(): boolean {
    return this.$store.state.game.currentPlayers < 2;
  }

  /**
   * The current route as displayed in the browser's address bar.
   * Needed to display the link to the current game.
   */
  get currentRoute(): string {
    return this.$route.path;
  }

  /**
   * Whether the current game was started in party (voice chat) mode.
   */
  get partyMode(): boolean {
    return this.$store.state.game.partyMode;
  }

  // Methods
  /**
   * Swiping left on touchscreen devices hides the player list / menu.
   * Exception: The player menu can't be hidden if the current user needs to select another player
   * to assign a character to.
   * @private
   */
  private onSwipeLeft() {
    if (!this.isSelecting) {
      this.showPlayerList = false;
    }
  }

  /**
   * Swiping right on touchscreen devices drags the player list / menu into view.
   * @private
   */
  private onSwipeRight() {
    this.showPlayerList = true;
  }

  /**
   * Starts the character assignment phase of the current game.
   * @private
   */
  private startSelecting() {
    this.$store.dispatch("startSelecting");
  }

  /**
   * Notifies the current user that the character assignment phase has begun and they need
   * to select another player to assign a character to.
   * @private
   */
  private startSelectionDialog() {
    this.$buefy.dialog.alert({
      title: this.$t("message.startSelectionDialogTitle").toString(),
      message: this.$t("message.startSelectionDialogMessage").toString(),
      type: "is-primary",
      scroll: "keep",
      ariaRole: "alertdialog",
      ariaModal: true,
      onConfirm: () => (this.showPlayerList = true) // The player menu appears when the user closes the dialogue
    });
  }

  /**
   * Asks the current user for a character they want to assign to the player they have selected
   * in the player menu.
   * @param player - A Player object describing the selected player
   * @private
   */
  private startSelectionPrompt(player: Player) {
    this.$buefy.dialog.prompt({
      type: "is-success",
      // Which character do you want to assign to <player name>?
      message: this.$t("message.startSelectionPromptMessage", [
        player.userName
      ]).toString(),
      inputAttrs: {
        type: "text",
        placeholder: this.$t(
          "message.startSelectionPromptPlaceholder"
        ).toString(),
        maxlength: 40,
        required: true
      },
      confirmText: this.$t("ui.accept").toString(),
      cancelText: this.$t("ui.cancel").toString(),
      trapFocus: true,
      scroll: "keep",
      onCancel: () => {
        this.cancelAssignment(player.userId);
      },
      onConfirm: (value: string) => this.assignCharacter(player.userId, value)
    });
  }

  /**
   * Shows the character selection modal in party mode, which asks the user to assign a character to the player
   * they have selected and optionally upload an image.
   * @param player - A Player object describing the selected player
   * @private
   */
  private startSelectionModal(player: Player) {
    this.$buefy.modal.open({
      canCancel: false,
      events: {
        assignCharacter: (data: { character: string; file: File | null }) => {
          if (data.file) {
            this.$store.dispatch("uploadImage", {
              file: data.file,
              uid: player.userId
            });
          }
          this.assignCharacter(player.userId, data.character);
        },
        cancelAssignment: () => {
          this.cancelAssignment(player.userId);
        }
      },
      parent: this,
      props: {
        player: player
      },
      component: SelectionModal,
      hasModalCard: true,
      trapFocus: true,
      scroll: "keep"
    });
  }

  /**
   * Assigns a character to the selected player.
   * @param uid - The UID of the player the character gets assigned to
   * @param character - The character
   * @private
   */
  private assignCharacter(uid: string, character: string) {
    this.showPlayerList = false;
    this.$store.dispatch("assignCharacter", {
      uid,
      character
    });
  }

  /**
   * Cancels an ongoing character assignment and un-reserves the selected player
   */
  private cancelAssignment(uid: string) {
    console.log("cancelled assignment");
    // When the user cancels the character assignment they need to select another player
    this.showPlayerList = true;
    this.$store.dispatch("stopCharacterAssignment", {
      uid: uid
    });
  }

  /**
   * Asks the user for a name and creates a new anonymous Firebase user.
   * @private
   */
  private startLoginPrompt() {
    this.$buefy.dialog.prompt({
      type: "is-primary",
      message: this.$t("message.startLoginPromptTitle").toString(),
      inputAttrs: {
        type: "text",
        placeholder: this.$t("ui.yourName").toString(),
        maxlength: 40,
        required: true
      },
      confirmText: this.$t("ui.accept").toString(),
      cancelText: this.$t("ui.back").toString(),
      trapFocus: true,
      scroll: "keep",
      canCancel: ["button"],
      onCancel: () => this.$router.push({ path: `/` }),
      onConfirm: userName => {
        if (!this.isLoggedIn) {
          this.signUp(userName);
        } else {
          // If the current user already has a Firebase user assigned to them, it's not necessary to create a new
          // one. Instead, just change the name of the old one and join the game under the new name.
          this.$store.state.user.updateProfile({
            displayName: userName
          });
          this.joinGame(this.$store.state.user.uid, userName);
        }
      }
    });
  }

  /**
   * Notifies the user that the current game has reached the maximum amount of players
   * and can't be joined anymore.
   * @private
   */
  private gameFullDialog() {
    this.$buefy.dialog.alert({
      title: this.$t("message.gameFullDialogTitle").toString(),
      message: this.$t("message.gameFullDialogMessage").toString(),
      type: "is-danger",
      scroll: "keep",
      ariaRole: "alertdialog",
      ariaModal: true,
      confirmText: this.$t("ui.back").toString(),
      onConfirm: () => this.$router.push({ path: `/` })
    });
  }

  /**
   * Creates a new anonymous Firebase user with a name and joins the current game under that name.
   * @param userName - The user name for the new Firebase user
   * @private
   */
  private signUp(userName: string) {
    firebase
      .auth()
      .signInAnonymously()
      .then(cred => {
        if (!cred.user) {
          return;
        }
        cred.user.updateProfile({
          displayName: userName
        });
        this.joinGame(cred.user.uid, userName);
      });
  }

  /**
   * Adds a user to the player list of the current game.
   * @param userId - The UID of the new player
   * @param userName - The display name of the new player
   * @private
   */
  private joinGame(userId: string, userName: string) {
    const index = this.$store.state.game.players.length;
    this.$store.dispatch("addUser", {
      index: index,
      uid: userId,
      userName: userName
    });
  }

  // Lifecycle hook
  /**
   * Determines whether a dialogue or prompt should be shown on page load.
   */
  created() {
    // If the game is full and the current user hasn't joined already, notify them that this game
    // can't be joined anymore
    if (!this.hasJoinedGame && this.isFull) {
      this.gameFullDialog();

      // If the game is not full but the current user hasn't joined yet, ask them for a name to join
    } else if (!this.hasJoinedGame) {
      this.startLoginPrompt();

      // If the character assignment has started and the current user hasn't assigned a character yet,
      // notify them that they need to do so
    } else if (this.selectedPlayer !== undefined && !this.hasAssigned) {
      if (this.partyMode) {
        this.startSelectionModal(this.selectedPlayer);
      } else {
        this.startSelectionPrompt(this.selectedPlayer);
      }

      // If the current user has "reserved" another player for character assignment, ask them for a character
    } else if (this.isSelecting) {
      this.startSelectionDialog();
    }
  }

  // Watchers
  @Watch("isSelecting")
  onIsSelecting(isSelecting: boolean, oldValue: boolean) {
    // Notify the user when the game creator has started the character assignment phase
    if (!oldValue && isSelecting && !this.hasSelected && this.hasJoinedGame) {
      this.startSelectionDialog();
      // Hide the player menu when the character assignment phase is completed
    } else if (!isSelecting) {
      this.showPlayerList = false;
    }
  }

  @Watch("selectedPlayer")
  onSelectedPlayer(
    selectedPlayer: Player | undefined,
    oldValue: Player | undefined
  ) {
    // Ask the current user for a character when they have selected a player
    if (selectedPlayer !== undefined && oldValue === undefined) {
      if (this.partyMode) {
        this.startSelectionModal(selectedPlayer);
      } else {
        this.startSelectionPrompt(selectedPlayer);
      }
    }
  }
}
</script>
