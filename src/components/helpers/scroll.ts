export function scrollToBottom(id: string, timeout: number) {
  setTimeout(function() {
    const scrollContainer = document.getElementById(id);
    if (scrollContainer) {
      const isScrolledToBottom =
        scrollContainer.scrollHeight - scrollContainer.clientHeight <=
        scrollContainer.scrollTop + 1;
      if (!isScrolledToBottom) {
        scrollContainer.scrollTop = scrollContainer.scrollHeight;
      }
    }
  }, timeout);
}
