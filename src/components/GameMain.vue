<template>
  <!-- The swipe gestures are handled by the top container so they work everywhere on the page -->
  <!-- The desktop version isn't affected by the swipe events -->
  <div v-touch:swipe.right="onSwipeRight" v-touch:swipe.left="onSwipeLeft">
    <!-- The popup darkens the background when either the player list or the question list is shown -->
    <!-- Clicking on the popup (i.e. anywhere other than the list) hides the list -->
    <div
      v-if="showPlayerList || showQuestionList"
      class="popup"
      @click="hideList"
    ></div>

    <!-- The main part of the game containing player list, current player, chat, and questions list -->
    <div class="tile is-ancestor game--main">
      <!-- Left column: player list -->
      <div
        class="tile is-parent"
        :class="{
          'side-left': isTouch,
          expanded: showPlayerList,
          'is-3': partyMode
        }"
      >
        <section class="tile is-child">
          <player-list />
        </section>
      </div>

      <!-- Center column: current player and optional image for party mode -->
      <div
        v-if="partyMode"
        class="tile is-parent is-vertical is-7 game--center party-mode"
      >
        <article class="tile is-child box current-player">
          <h1 class="title is-spaced">{{ player.userName }}</h1>
          <h2
            class="subtitle"
            v-if="player && (isNotPlayer || player.hasSolved)"
          >
            {{ player.character }}
          </h2>
          <h2 class="subtitle player-character" v-else>
            {{ $t("ui.character") }}
          </h2>
          <img
            v-if="imageName !== undefined && (isNotPlayer || player.hasSolved)"
            id="character-image"
            class="character-image"
            src=""
            :alt="$t('ui.characterImage')"
          />
          <img
            v-else-if="imageName !== undefined"
            class="player-image"
            src="../../public/favicon.svg"
            :alt="$t('ui.characterImage')"
          />
        </article>
        <div class="buttons">
          <b-button
            type="is-hasSolved"
            size="is-large"
            class="button-solve"
            @click="nextPlayer('2')"
            :disabled="isNotPlayer"
            outlined
          >
            {{ $t("ui.solve") }}
          </b-button>
          <b-button
            v-if="isTouch"
            type="is-primary"
            size="is-large"
            class="button-next"
            @click="nextPlayer('1')"
            :disabled="isNotPlayer || isLastPlayer"
            outlined
            style="white-space: pre-line"
          >
            {{ $t("ui.nextMobile") }}
          </b-button>
          <b-button
            v-else
            type="is-primary"
            size="is-large"
            class="button-next"
            @click="nextPlayer('1')"
            :disabled="isNotPlayer || isLastPlayer"
            outlined
          >
            {{ $t("ui.next") }}
          </b-button>
        </div>
      </div>

      <!-- Center column: current player and chat for chat mode -->
      <div
        v-else
        class="tile is-parent is-vertical is-7 game--center chat-mode"
      >
        <article class="tile is-child box current-player">
          <h1 class="title is-spaced">{{ player.userName }}</h1>
          <h2
            class="subtitle"
            v-if="player && (isNotPlayer || player.hasSolved)"
          >
            {{ player.character }}
          </h2>
          <h2 class="subtitle player-character" v-else>
            {{ $t("ui.character") }}
          </h2>
        </article>
        <section class="tile is-child">
          <game-chat />
        </section>
      </div>

      <!-- Right column: question list for chat mode -->
      <div
        v-if="!partyMode"
        class="tile is-parent"
        :class="{ 'side-right': isTouch, expanded: showQuestionList }"
      >
        <section class="tile is-child">
          <question-list />
        </section>
      </div>
    </div>
  </div>
</template>

<script lang="ts">
import { Component, Watch, Vue } from "vue-property-decorator";
import { Player } from "@/types";
import { storage } from "@/firebase";
import GameChat from "@/components/GameChat.vue";
import PlayerList from "@/components/PlayerList.vue";
import QuestionList from "@/components/QuestionList.vue";

@Component({
  components: {
    GameChat,
    PlayerList,
    QuestionList
  }
})
export default class GameMain extends Vue {
  // Data
  /**
   * Whether the player list is dragged into view on touchscreen devices.
   * @private
   */
  private showPlayerList = false;
  /**
   * Whether the question list is dragged into view on touchscreen devices.
   * @private
   */
  private showQuestionList = false;

  // Computed properties
  /**
   * Whether the page is being viewed on a touchscreen device (mobile or tablet).
   */
  get isTouch(): boolean {
    return this.$store.state.isTouch;
  }

  /**
   * Returns the players in this game as an array.
   */
  get players(): Array<Player> {
    return Object.entries(this.$store.state.game.players).map(
      x => x[1] as Player
    );
  }

  /**
   * Returns the player whose turn it is.
   */
  get player(): Player {
    return this.players.filter(
      player => player.userId === this.$store.state.game.playerId
    )[0];
  }

  /**
   * Whether the current user is not the current player.
   */
  get isNotPlayer(): boolean {
    try {
      return this.$store.state.game.playerId !== this.$store.state.user.uid;
    } catch {
      return true;
    }
  }

  /**
   * Whether the current game has been finished, i.e. all player have guessed their character.
   */
  get isFinished(): boolean {
    return this.$store.state.game.isFinished;
  }

  /**
   * Whether the current user is the host of the game and as such, can restart it
   */
  get isHost(): boolean {
    const game = this.$store.state.game;
    return this.userId === game.creatorId;
  }

  /**
   * Returns the UID of the current user
   */
  get userId(): string | undefined {
    if (this.isLoggedIn) {
      return this.$store.state.user.uid;
    } else {
      return undefined;
    }
  }

  /**
   * Whether the current user is logged in, i.e. an anonymous Firebase user has been created.
   */
  get isLoggedIn(): boolean {
    return this.$store.state.user !== null;
  }

  /**
   * Whether the current game was started in party (voice chat) mode.
   */
  get partyMode(): boolean {
    return this.$store.state.game.partyMode;
  }

  /**
   * Returns the name of the character image, if one was uploaded.
   */
  get imageName(): string | undefined {
    return this.player.image;
  }

  /**
   * Returns the ID of the current game.
   */
  get gameId(): string {
    return this.$store.state.game[".key"];
  }

  /**
   * Whether the current player is the last one to guess their character
   */
  get isLastPlayer(): boolean {
    return this.players.filter(player => !player.hasSolved).length <= 1;
  }

  // Methods
  /**
   * Swiping left drags the question list into view OR hides the player list if the latter is visible.
   * @private
   */
  private onSwipeLeft() {
    if (!this.isTouch) {
      return;
    }
    if (this.showPlayerList) {
      this.showPlayerList = false;
    } else {
      this.showQuestionList = true;
    }
  }

  /**
   * Swiping right drags the player list into view OR hides the question list if the latter is visible.
   * @private
   */
  private onSwipeRight() {
    if (!this.isTouch) {
      return;
    }
    if (this.showQuestionList) {
      this.showQuestionList = false;
    } else {
      this.showPlayerList = true;
    }
  }

  /**
   * Hides both the player list and the question list.
   * @private
   */
  private hideList() {
    this.showPlayerList = false;
    this.showQuestionList = false;
  }

  /**
   * Notifies the current user that the current game has been finished.
   * @private
   */
  private finishedGameDialog(): void {
    this.$buefy.dialog.alert({
      title: this.$t("message.finishedGameDialogTitle").toString(),
      message: this.$t("message.finishedGameDialogMessage").toString(),
      type: "is-danger",
      scroll: "keep",
      ariaRole: "alertdialog",
      ariaModal: true,
      confirmText: this.$t("ui.leave").toString(),
      onConfirm: () => this.$router.push({ path: `/` })
    });
  }

  /**
   * Notifies the current user that the current game has been finished.
   * @private
   */
  private finishedGameHostDialog(): void {
    this.$buefy.dialog.confirm({
      title: this.$t("message.finishedGameDialogTitle").toString(),
      message: this.$t("message.finishedGameHostDialogMessage").toString(),
      type: "is-success",
      scroll: "keep",
      ariaRole: "alertdialog",
      ariaModal: true,
      cancelText: this.$t("ui.leave").toString(),
      confirmText: this.$t("ui.restart").toString(),
      onCancel: () => this.$router.push({ path: `/` }),
      onConfirm: () => this.$store.dispatch("restartGame")
    });
  }

  /**
   * Gets the image for the current player from Firebase and updates the image source accordingly.
   * @private
   */
  private getImageURL(imageName: string) {
    console.log("getImageURL called");
    storage
      .ref("images/" + this.gameId)
      .child(imageName)
      .getDownloadURL()
      .then(function(url) {
        const img = document.getElementById(
          "character-image"
        ) as HTMLImageElement;
        img.src = url;
      });
  }

  /**
   * Tell the server to find the next player
   * Votes: 1 = player hasn't guessed their character
   * 2 = player has guessed their character
   */
  private nextPlayer(vote: string) {
    this.$store.dispatch("nextPlayer", vote);
  }

  // Watcher
  /**
   * When the server determines that the game is finished, notify the user.
   * @param isFinished
   */
  @Watch("isFinished")
  onFinished(isFinished: boolean) {
    if (isFinished) {
      this.isHost ? this.finishedGameHostDialog() : this.finishedGameDialog();
    } else {
      const dialog = document.getElementsByClassName("dialog")[0];
      dialog.remove();
    }
  }

  /**
   * When the player becomes the new host, he gets the option to restart the game
   */
  @Watch("isHost")
  onHost(isHost: boolean){
    if (this.isFinished && isHost) {
      const dialog = document.getElementsByClassName("dialog")[0];
      dialog.remove();
      this.finishedGameHostDialog()
    }
  }

  /**
   * Reloads the character image whenever the current player changes.
   */
  @Watch("player")
  onPlayer() {
    console.log("onPlayer called");
    if (this.imageName !== undefined) this.getImageURL(this.imageName);
  }
}
</script>
